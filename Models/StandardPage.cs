using Piranha.AttributeBuilder;
using Piranha.Models;

namespace themimicshop.Models
{
    [PageType(Title = "Standard page")]
    public class StandardPage  : Page<StandardPage>
    {
    }
}